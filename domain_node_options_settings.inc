<?php

/**
 * Form builder function for module settings.
 */
function domain_node_options_admin_settings() {
  $form['domain_node_options_settings'] = array(
    '#type' => 'fieldset', 
    '#title' => t('Node types'), 
    '#collapsible' => TRUE, 
    '#collapsed' => FALSE,
    '#description' => t('Active this functionality on these node types.'),
  );

  foreach (node_get_types() AS $type) {
    $form['domain_node_options_settings']['domain_node_options_'. $type->type] = array(
      '#type' => 'checkbox', 
      '#title' => $type->name,
      '#default_value' => variable_get('domain_node_options_'. $type->type, 0),
    );
  }

  return system_settings_form($form);
}